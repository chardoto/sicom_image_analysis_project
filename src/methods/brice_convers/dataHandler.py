import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from src.forward_model import CFA
from pathlib import Path
from src.methods.baseline.reconstruct import run_reconstruction
from src.methods.brice_convers.reconstruct import run_reconstruction as run_reconstruction_brice_convers
from src.methods.brice_convers.utilities import folderExists
from src.utils import normalise_image, save_image, psnr, ssim
from skimage.io import imread
import numpy as np

class DataHandler:
    def __init__(self, workingDirectoryPath = ""):
        DataHandler.imagePaths =[]
        DataHandler.imagePathsLabels = []
        DataHandler.CFA_images = {}
        DataHandler.reconstruction_images = {"interpolation": {}, "menon": {}}

        DataHandler.IMAGE_TYPES = (".jpg", ".jpeg", ".png", ".bmp", ".tif", ".tiff")
        DataHandler.WD_PATH = workingDirectoryPath

    def list_files(self, basePath, interval, validExts=None, contains=None):
        sum = 0

        if interval is not None:
            counter = interval[0]
            lastFile = interval[1]
        else:
            counter = 0
            lastFile = -1

        # loop over the directory structure
        for (rootDir, dirNames, filenames) in os.walk(basePath):
            # loop over the filenames in the current directory
            for filename in filenames:
                # if the contains string is not none and the filename does not contain
                # the supplied string, then ignore the file
                if contains is not None and filename.find(contains) == -1:
                    continue

                # determine the file extension of the current file
                ext = filename[filename.rfind("."):].lower()

                # check to see if the file is an image and should be processed
                if validExts is None or ext.endswith(validExts):
                    if sum >= counter and (sum <= lastFile) or lastFile == -1:
                        # construct the path to the image and yield it
                        imagePath = os.path.join(rootDir, filename)
                        imageName = Path(imagePath).stem
                        DataHandler.imagePaths.append(imagePath)
                        DataHandler.imagePathsLabels.append(imageName)

                    sum += 1

    def list_images(self, basePath, interval = None, contains=None):
        # return the set of files that are valid
        DataHandler.list_files(self, basePath, interval, validExts=DataHandler.IMAGE_TYPES, contains=contains)
        print("[INFO] There are {} images.".format(len(DataHandler.imagePaths)))

    def print_list_images(self):
        print("[INFO] This is the order or your image in the list. IndexImage is the index in this table:")
        print(DataHandler.imagePathsLabels)

    def plot_input_images(self, rows=3, cols=3, figsize=(15, 5), max_images = 6, title="Input Images"):
        fig = plt.figure(figsize=figsize)
        fig.suptitle(title, fontsize=16)

        for i, imagePath in enumerate(DataHandler.imagePaths):
            if i >= max_images:
                break

            img = mpimg.imread(imagePath)
            fig.add_subplot(rows, cols, i+1)
            # image title
            plt.title(imagePath.split(os.path.sep)[-1])
            plt.text(0, -0.1, f"Shape of the image: {img.shape}.", fontsize=12, transform=plt.gca().transAxes)
            plt.imshow(img)

        plt.show()

    def plot_raw_transformation(self, zoom = True, specificIndex = None, rows=8, cols=3, figsize=(15, 5), max_images = 24, title="Raw Transformation"):
        fig, axs = plt.subplots(rows, cols, figsize=figsize)
        fig.suptitle(title, fontsize=16)

        for i, imagePath in enumerate(DataHandler.imagePaths):
            if i >= max_images and max_images != -1:
                break

            if specificIndex is not None:
                if i != specificIndex:
                    continue

            nameImage = Path(imagePath).stem

            if DataHandler.CFA_images.get(nameImage) is None:
                print("[ERROR] There is no CFA image for the image {}.".format(nameImage))
                continue

            img = mpimg.imread(imagePath)

            y = DataHandler.CFA_images[nameImage].direct(img)
            z = DataHandler.CFA_images[nameImage].adjoint(y)

            if specificIndex is None:
                line = 2*i
                subLine = 2*i+1
            else:
                line = 0
                subLine = 1

            axs[line, 0].imshow(img)
            axs[line, 0].set_title('Input image')
            axs[line, 1].imshow(y, cmap='gray')
            axs[line, 1].set_title('Output image')
            axs[line, 2].imshow(z)
            axs[line, 2].set_title('Adjoint image')

            if zoom:
                axs[subLine, 0].imshow(img[800:864, 450:514])
                axs[subLine, 0].set_title('Zoomed input image')
                axs[subLine, 1].imshow(y[800:864, 450:514], cmap='gray')
                axs[subLine, 1].set_title('Zoomed output image')
                axs[subLine, 2].imshow(z[800:864, 450:514])
                axs[subLine, 2].set_title('Zoomed adjoint image')

        print("[INFO] There are {} ploted images.".format(max_images))

    def plot_specific_raw_transformation(self, indexImage, zoom = True, rows=2, cols=3, figsize=(15, 5), title="Raw Transformation"):
        DataHandler.plot_raw_transformation(self, zoom, indexImage, rows, cols, figsize, -1, title)

    def compute_CFA_images(self, CFA_NAME):
        for i, imagePath in enumerate(DataHandler.imagePaths):
            nameImage = Path(imagePath).stem
            DataHandler.compute_CFA_image(self, CFA_NAME, nameImage, i)

        print("[INFO] There are {} CFA images.".format(len(DataHandler.CFA_images)))

    def compute_reconstruction_image(self, method, indexImage, options = None):
        if len(DataHandler.imagePaths) == 0:
            print("[ERROR] There is no image in imagePaths")
            return

        # Test key method
        if method not in DataHandler.reconstruction_images.keys():
            print("[ERROR] The method {} is not valid.".format(method))
            exit(1)

        nameImage = Path(DataHandler.imagePaths[indexImage]).stem

        if DataHandler.CFA_images.get(nameImage) is None:
            print("[ERROR] There is no CFA image for the image {}.".format(nameImage))
            exit(1)

        img = DataHandler.load_image(self, indexImage)

        img_CFA = DataHandler.CFA_images[nameImage].direct(img)

        cfa = options.get("cfa")

        if cfa is None:
            print("[ERROR] You must specify the cfa.")
            exit(1)

        if method == "interpolation":

            DataHandler.reconstruction_images[method].setdefault(nameImage, run_reconstruction(img_CFA, cfa))

        if method == "menon":

            DataHandler.reconstruction_images[method].setdefault(nameImage, run_reconstruction_brice_convers(img_CFA, cfa))

    def compute_reconstruction_images(self, method, options = None):

        for i in range(len(DataHandler.imagePaths)):
            DataHandler.compute_reconstruction_image(self, method, i, options)

        print("[INFO] There are {} images which have been reconstructed.".format(len(DataHandler.reconstruction_images[method])))

    def plot_reconstructed_image(self, indexImage, method, cfa = {"cfa": "bayer"}, zoomSize = "small", rows=1, cols=4, figsize=(15, 5)):
        # Test key method
        if method not in DataHandler.reconstruction_images.keys():
            print("[ERROR] The method {} is not valid.".format(method))
            exit(1)

        res = DataHandler.get_reconstructed_image(self, indexImage, method)

        fig, axs = plt.subplots(rows, cols, figsize=figsize)
        fig.suptitle("Reconstructed Image with method: {} and pattern type: {}".format(method, cfa["cfa"]), fontsize=16)

        axs[0].imshow(DataHandler.load_image(self, indexImage))
        axs[0].set_title('Original Image')
        axs[1].imshow(res)
        axs[1].set_title('Reconstructed Image')

        if zoomSize == "small":
            axs[2].imshow(DataHandler.load_image(self, indexImage)[800:864, 450:514])
            axs[2].set_title('Zoomed Input Image')
            axs[3].imshow(res[800:864, 450:514])
            axs[3].set_title('Zoomed Reconstructed Image')
        else:
            axs[2].imshow(DataHandler.load_image(self, indexImage)[2000:2064, 2000:2064])
            axs[2].set_title('Zoomed Input Image')
            axs[3].imshow(res[2000:2064, 2000:2064])
            axs[3].set_title('Zoomed Reconstructed Image')

        outputPath = os.path.join(DataHandler.WD_PATH, "output")
        folderExists(outputPath)

        imageName = Path(DataHandler.imagePaths[indexImage]).stem
        plotCompImagePath = os.path.join(outputPath, "Compararison_" + imageName + "_" + method + "_" + cfa["cfa"] + ".png")

        #save_image( plotReconstructedImagePath, res)
        fig.savefig(plotCompImagePath)


    def get_reconstructed_image(self, indexImage, method):
        # Test key method
        if method not in DataHandler.reconstruction_images.keys():
            print("[ERROR] The method {} is not valid.".format(method))
            exit(1)

        DataHandler.indexImageExists(self, indexImage)

        nameImage = Path(DataHandler.imagePaths[indexImage]).stem

        if DataHandler.reconstruction_images[method].get(nameImage) is None:
            print("[ERROR] There is no reconstruction image for the image {} and for the method {}.".format(nameImage, method))
            exit(1)

        return DataHandler.reconstruction_images[method][nameImage]

    def shape_of_recontructed_image(self, indexImage, method):
        if DataHandler.reconstruction_images[method].get(Path(DataHandler.imagePaths[indexImage]).stem) is None:
            print("[ERROR] There is no reconstruction image for the image {} and for the method {}.".format(Path(DataHandler.imagePaths[indexImage]).stem, method))
            exit(1)

        return DataHandler.reconstruction_images[method][Path(DataHandler.imagePaths[indexImage]).stem].shape

    def compute_CFA_image(self, CFA_NAME, nameImage, indexImage):
        DataHandler.CFA_images.setdefault(nameImage, CFA(CFA_NAME, DataHandler.shape_of_image(self, indexImage)))

    def shape_of_image(self, indexImage):
        img = mpimg.imread(DataHandler.imagePaths[indexImage])
        return img.shape

    def load_image(self, indexImage):
        img = imread(DataHandler.imagePaths[indexImage])
        img = normalise_image(img)
        return img

    def indexImageExists(self, indexImage):
        if indexImage >= len(DataHandler.imagePaths):
            print("[ERROR] The index {} is not valid.".format(indexImage))
