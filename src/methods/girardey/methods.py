import numpy as np
from src.forward_model import CFA

def HQ_interpolation(op: CFA, y: np.ndarray) -> np.ndarray:
    """Performs a simple interpolation of the lost pixels.

    Args:
        op (CFA): CFA operator.
        y (np.ndarray): Mosaicked image.

    Returns:
        np.ndarray: Demosaicked image.
    """
    z = op.adjoint(y)

    if op.cfa == 'bayer':
        res = np.empty(op.input_shape)

        res[:, :, 0] = varying_kernel_convolution_HQ(y[:, :], K_red_list)
        res[:, :, 1] = varying_kernel_convolution_HQ(y[:, :], K_green_list)
        res[:, :, 2] = varying_kernel_convolution_HQ(y[:, :], K_blue_list)
 
    else:
        res = np.empty(op.input_shape)

        bayer_y = quad_bayer_to_bayer(y)
        res[:, :, 0] = varying_kernel_convolution_HQ(bayer_y[:, :], K_red_list)
        res[:, :, 1] = varying_kernel_convolution_HQ(bayer_y[:, :], K_green_list)
        res[:, :, 2] = varying_kernel_convolution_HQ(bayer_y[:, :], K_blue_list)

    return res

def extract_padded(M, size, i, j):
    N_i, N_j = M.shape
    res = np.zeros((size, size))
    middle_size = int((size - 1) / 2)

    for ii in range(- middle_size, middle_size + 1):
        for jj in range(- middle_size, middle_size + 1):
            if i + ii >= 0 and i + ii < N_i and j + jj >= 0 and j + jj < N_j:
                res[middle_size + ii, middle_size + jj] = M[i + ii, j + jj]

    return res


def varying_kernel_convolution(M, K_list):
    N_i, N_j = M.shape
    res = np.zeros_like(M)

    for i in range(N_i):
        for j in range(N_j):
            res[i, j] = np.sum(extract_padded(M, K_list[4 * (i % 4) + j % 4].shape[0], i, j) * K_list[4 * (i % 4) + j % 4])

    np.clip(res, 0, 1, res)

    return res

def varying_kernel_convolution_HQ(M, K_list):

    res = np.zeros_like(M)
    M_padded = np.pad(M,2,"constant",constant_values=0)
    N_i, N_j = M_padded.shape
    for i in range(2,N_i-2):
        for j in range(2,N_j-2):
            res[i-2, j-2] = np.sum(extract_padded(M_padded, 5, i ,j) * K_list[2* (i % 2) + j % 2])

    np.clip(res, 0, 1, res)

    return res

def quad_bayer_to_bayer(y):
    res = y
    N_i, N_j = y.shape
    for j in range(int(N_j/4)):
        buff = res[:,j*4+1]
        res[:,j*4+1] = res[:,j*4+2]
        res[:,j*4+2] = buff
    for i in range(int(N_i/4)):
        buff = res[i*4+1,:]
        res[i*4+1,:] = res[i*4+2,:]
        res[i*4+2,:] = buff
    for i in range(int(N_i/4)):
        for j in range(int(N_j/4)):
            buff = res[i*4+2,j*4+1]
            res[i*4+2,j*4+1] = res[i*4+1,j*4+2]
            res[i*4+1,j*4+2] = buff   
    return res



K_identity = np.zeros((5,5))
K_identity[2,2] = 1

K_g_r_and_b = np.array([[0, 0, -1, 0, 0], [0, 0, 2, 0, 0], [-1, 2, 4, 2, -1], [0, 0, 2, 0, 0], [0, 0, -1, 0, 0]]) / 8

K_r_and_b_g_rrow_bcol = np.array([[0, 0, 1/2, 0, 0], [0, -1, 0, -1, 0], [-1, 4, 5, 4, -1], [0, -1, 0, -1, 0], [0, 0, 1/2, 0, 0]]) / 8
K_r_and_b_g_brow_rcol = K_r_and_b_g_rrow_bcol.T
K_r_b = np.array([[0, 0, -3/2, 0, 0], [0, 2, 0, 2, 0], [-3/2, 0, 6, 0, -3/2], [0, 2, 0, 2, 0], [0, 0, -3/2, 0, 0]]) / 8

K_green_list = [K_identity, 
                K_g_r_and_b,
                K_g_r_and_b,
                K_identity]

K_red_list = [K_r_and_b_g_rrow_bcol,
              K_identity,
              K_r_b,
              K_r_and_b_g_brow_rcol]

K_blue_list = [K_r_and_b_g_brow_rcol,
               K_r_b,
               K_identity,
               K_r_and_b_g_rrow_bcol]