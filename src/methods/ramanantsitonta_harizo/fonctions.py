import numpy as np 
from src.forward_model import CFA
import cv2 as cv2

def hamilton_adams(y, input_shape):
    n,p = input_shape[0], input_shape[1]
    z = np.copy(y)
    for i in range(2 , n-2): #green interpolation by gradient comparison for every red and blue pixels
        for j in range(2, p-2):
            if z[i,j,1] == 0 and z[i,j,0] != 0:   #red pixel
                #Vertical and horizontal gradient
                grad_y = np.abs(z[i-1,j,1] - z[i+1,j,1]) + np.abs(2*z[i,j,0] - z[i-2,j,0] - z[i+2,j,0])
                grad_x = np.abs(z[i,j-1,1] - z[i,j+1,1]) + np.abs(2*z[i,j,0] - z[i,j-2,0] - z[i,j+2,0])

                if grad_x < grad_y:
                    z[i,j,1] = (z[i,j-1,1] + z[i,j+1,1])/2 + (2*z[i,j,0] - z[i,j-2,0] - z[i,j+2,0])/4
                elif  grad_x > grad_y:
                    z[i,j,1] = (z[i-1,j,1] + z[i+1,j,1])/2 + (2*z[i,j,0] - z[i-2,j,0] - z[i+2,j,0])/4
                else:
                    z[i,j,1] = (z[i-1,j,1] + z[i+1,j,1] + z[i,j-1,1] + z[i,j+1,1])/4 + (2*z[i,j,0] - z[i,j-2,0] - z[i,j+2,0] + 2*z[i,j,0] - z[i-2,j,0] - z[i+2,j,0])/8
                
            elif z[i,j,1] == 0 and z[i,j,2] != 0:    #blue pixel
                #Vertical and horizontal gradient
                grad_y = np.abs(z[i-1,j,1] - z[i+1,j,1]) + np.abs(2*z[i,j,2] - z[i-2,j,2] - z[i+2,j,2])
                grad_x = np.abs(z[i,j-1,1] - z[i,j+1,1]) + np.abs(2*z[i,j,2] - z[i,j-2,2] - z[i,j+2,2])

                if grad_x < grad_y:
                    z[i,j,1] = (z[i,j-1,1] + z[i,j+1,1])/2 + (2*z[i,j,2] - z[i,j-2,2] - z[i,j+2,2])/4
                elif  grad_x > grad_y:
                    z[i,j,1] = (z[i-1,j,1] + z[i+1,j,1])/2 + (2*z[i,j,2] - z[i-2,j,2] - z[i+2,j,2])/4
                else:
                    z[i,j,1] = (z[i-1,j,1] + z[i+1,j,1] + z[i,j-1,1] + z[i,j+1,1])/4 + (2*z[i,j,2] - z[i,j-2,2] - z[i,j+2,2] + 2*z[i,j,2] - z[i-2,j,2] - z[i+2,j,2])/8
        
    for i in range(1 , n-1): #red/blue interpolation by bilinear interpolation on blue/red pixels
        for j in range(1, p-1):
            if z[i,j,2] != 0 :
                 z[i,j,0] = (z[i-1,j-1,0] + z[i-1,j+1,0] + z[i+1,j-1,0] + z[i+1,j+1,0]) / 4

            elif z[i,j,0] != 0:
                 z[i,j,2] = (z[i-1,j-1,2] + z[i-1,j+1,2] + z[i+1,j-1,2] + z[i+1,j+1,2]) / 4
            
            else:
                z[i,j] = z[i,j]

    for i in range(1 , n-1): #blue and red interpolation by bilinear interpolation on green pixels
        for j in range(1, p-1):    
            if z[i,j,0] == z[i,j,2]:
                z[i,j,0] = (z[i-1,j,0] + z[i,j-1,0] + z[i+1,j,0] + z[i,j+1,0]) / 4             
                z[i,j,2] = (z[i-1,j,2] + z[i,j-1,2] + z[i+1,j,2] + z[i,j+1,2]) / 4  
            
    return z

# def SSD(y, cfa_img, input_shape):  #SSD algo
#     n,p = input_shape[0], input_shape[1]
#     hlist = [16,4,1]
#     res = np.copy(y)
#     for h in hlist:
#         res = NLh(res, cfa_img ,n,p,h)
#         res = CR(res,n,p)
    
#     return res

# def NLh(y, cfa_img, n, p, h):  #NLh part
#     res = np.copy(cfa_img)
#     for i in range(4,n-3):
#         for j in range(4,p-3):
#             if cfa_img[i,j,0] != 0:
#                 res[i,j,1] = NLh_calc(y, cfa_img, i, j, 1, h)
#                 res[i,j,2] = NLh_calc(y, cfa_img, i, j, 2, h)
#                 print((i,j))

#             elif cfa_img[i,j,1] != 0:
#                 res[i,j,0] = NLh_calc(y, cfa_img, i, j, 0, h)
#                 res[i,j,2] = NLh_calc(y, cfa_img, i, j, 2, h)
#                 print((i,j))

#             else:
#                 res[i,j,0] = NLh_calc(y, cfa_img, i, j, 0, h)
#                 res[i,j,1] = NLh_calc(y, cfa_img, i, j, 1, h)
#                 print((i,j))

#     return res

# def NLh_calc(y, cfa_img, i, j, channel, h):   #aux function to calculate the main sum for each pixel
#     sum = 0
#     norm = 0
#     for k in range(-2,3):
#         for l in range(-2,3):
#             if k!=0 and j!=0:
#                 a = poids(y, channel, i,j,k,l,h)
#                 sum += a * cfa_img[i+k,j+l,channel]
#                 norm += a

#     return sum / norm

# def poids(y, channel, i,j,k,l,h):  #aux function to calcultate the weight for a given p=(i,j) , q=(k,l)
#     som = 0
#     # for tx in range(-1,2):
#     #     for ty in range(-1,2):

#     som += (np.abs(y[i-1 , j-1, channel] - y[k+1 , l-1, channel]))**2
#     som += (np.abs(y[i-1 , j, channel] - y[k+1 , l, channel]))**2
#     som += (np.abs(y[i-1 , j+1, channel] - y[k+1 , l+1, channel]))**2

#     som += (np.abs(y[i, j-1, channel] - y[k, l-1, channel]))**2
#     som += (np.abs(y[i, j, channel] - y[k, l, channel]))**2
#     som += (np.abs(y[i, j+1, channel] - y[k, l+1, channel]))**2   

#     som += (np.abs(y[i+1 , j-1, channel] - y[k+1 , l-1, channel]))**2
#     som += (np.abs(y[i+1 , j, channel] - y[k+1 , l, channel]))**2
#     som += (np.abs(y[i+1 , j+1, channel] - y[k+1 , l+1, channel]))**2


#     res = np.exp((-1/h**2) * som)
#     return res

# def CR(img_rgb):  #Chrominance median 
#     res = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2YUV)
#     y, u, v = cv2.split(res)
#     U_med = cv2.medianBlur(u, 3)
#     V_med = cv2.medianBlur(v, 3)

#     y = cv2.cvtColor(y, cv2.COLOR_GRAY2RGB)
#     u = cv2.cvtColor(u, cv2.COLOR_GRAY2RGB)
#     v = cv2.cvtColor(v, cv2.COLOR_GRAY2RGB)

#     res = np.vstack([y, u, v])

#     return res


