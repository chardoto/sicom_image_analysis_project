def interpolate_neighboring(neighbors_0: list, neighbors_1: list, weights: list) -> float:
    """
    Interpolate the neighboring pixels.
    
    Args:
        neighbors_0 (list): The neighbors of the pixel in the channel to process.
        neighbors_1 (list): The neighbors of the pixel in the green channel.
        weights (list): The weights of the direct neighbors.
        
    Returns:
        float: The interpolated value.
    """
    
    return neighbors_1[4] * (((weights[0] * neighbors_0[0]) / neighbors_1[0]) + ((weights[2] * neighbors_0[2]) / neighbors_1[2]) + ((weights[5] * neighbors_0[6]) / neighbors_1[6]) + ((weights[7] * neighbors_0[8]) / neighbors_1[8])) / (weights[0] + weights[2] + weights[5] + weights[7])
    