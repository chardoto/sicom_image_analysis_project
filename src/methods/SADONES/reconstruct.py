"""The main file for the reconstruction.
This file should NOT be modified except the body of the 'run_reconstruction' function.
Students can call their functions (declared in others files of src/methods/your_name).
"""


import numpy as np
import matplotlib.pyplot as plt
from local_int import local_int
from forward_model import CFA
from demo_reconstruction import naive_interpolation

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    if cfa == "quad_bayer":
        input_shape = (y.shape[0], y.shape[1], 3)
        op = CFA(cfa, input_shape)

        return naive_interpolation(op, y)

    # Performing the reconstruction.
    cfa_obj = CFA(cfa, y.shape)
    cfa_mask = cfa_obj.mask
    R_0, G_0, B_0 = local_int(y, 0.7, 12, 1e-8, cfa_mask)
    return np.stack((R_0, G_0, B_0), axis=2)

####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller
