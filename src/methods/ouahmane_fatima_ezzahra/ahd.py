import numpy as np
from scipy.ndimage import correlate


def ahd_demosaicking(op, y):
    z = op.adjoint(y)

    if op.cfa == 'bayer':
        
        res_red = correlate(z[:, :, 0], np.array([[0, 1, 0], [1, 4, 1], [0, 1, 0]]) / 4, mode='reflect')
        res_blue = correlate(z[:, :, 2], np.array([[0, 1, 0], [1, 4, 1], [0, 1, 0]]) / 4, mode='reflect')
        res_green = z[:, :, 1]

        res_red = np.clip(res_red, 0, 1)
        res_blue = np.clip(res_blue, 0, 1)
        demosaicked_image = np.stack([res_red, res_green, res_blue], axis=-1)

    else:
        demosaicked_image = np.empty(op.input_shape)

    return  np.clip(demosaicked_image * 1.5, 0, 1)
