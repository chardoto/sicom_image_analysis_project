"""The main file for the baseline reconstruction.
This file should NOT be modified.
"""


import numpy as np

from src.forward_model import CFA
from src.methods.baseline.demo_reconstruction import naive_interpolation
from tensorflow.keras.models import load_model

def run_reconstruction(y: np.ndarray, cfa: str) -> np.ndarray:
    """Performs demosaicking on y.

    Args:
        y (np.ndarray): Mosaicked image to be reconstructed.
        cfa (str): Name of the CFA. Can be bayer or quad_bayer.

    Returns:
        np.ndarray: Demosaicked image.
    """
    input_shape = (y.shape[0], y.shape[1], 3)
    op = CFA(cfa, input_shape)
    z = op.adjoint(y)
    model = load_model("model")
    predictions = model.predict(z)

    return predictions


####
####
####

####      ####                ####        #############
####      ######              ####      ##################
####      ########            ####      ####################
####      ##########          ####      ####        ########
####      ############        ####      ####            ####
####      ####  ########      ####      ####            ####
####      ####    ########    ####      ####            ####
####      ####      ########  ####      ####            ####
####      ####  ##    ######  ####      ####          ######
####      ####  ####      ##  ####      ####    ############
####      ####  ######        ####      ####    ##########
####      ####  ##########    ####      ####    ########
####      ####      ########  ####      ####
####      ####        ############      ####
####      ####          ##########      ####
####      ####            ########      ####
####      ####              ######      ####

# 2023
# Authors: Mauro Dalla Mura and Matthieu Muller


import tensorflow as tf
from tensorflow.keras import layers, models
import numpy as np
import matplotlib.pyplot as plt


with open('array.npy', 'rb') as f:
    tableau_image_or = np.load(f)
with open('array_z.npy', 'rb') as f:
    tableau_images = np.load(f)
    
X = tableau_images.transpose((3,0,1,2))
y = tableau_image_or.transpose((3,0,1,2))

# Création du modèle
model = models.Sequential()

# Encoder
model.add(layers.Conv2D(32, (3, 3), activation='relu', padding='same', input_shape=(1024, 1024, 3)))
model.add(layers.MaxPooling2D((2, 2), padding='same'))

# Decoder
model.add(layers.Conv2D(32, (3, 3), activation='relu', padding='same'))
model.add(layers.UpSampling2D((2, 2)))
model.add(layers.Conv2D(3, (3, 3), activation='sigmoid', padding='same'))

# Compiler le modèle
model.compile(optimizer='adam', loss='mse')  # Mean Squared Error comme fonction de perte

# Entraînement du modèle
model.fit(X, y, epochs=3, batch_size=10, validation_split=0.2)

# Évaluation du modèle
loss = model.evaluate(X, y)
print(f"Loss sur l'ensemble de données : {loss}")

# Faire des prédictions
predictions = model.predict(X)

# Afficher quelques exemples
for i in range(5):  # Afficher les 5 premières images par exemple
    plt.figure(figsize=(10, 5))

    # Image originale
    plt.subplot(1, 3, 1)
    plt.imshow(y[i])
    plt.title('Image Originale')

    # Image avec pixels masqués
    plt.subplot(1, 3, 2)
    plt.imshow(X[i])
    plt.title('Image avec Pixels Masqués')

    # Image prédite
    plt.subplot(1, 3, 3)
    plt.imshow(predictions[i])
    plt.title('Image Prédite')

    plt.show()